import tensorflow as tf
import numpy as np
import tensorflow.contrib.layers as layers

num_actions = 10

def _mlp(hiddens, inpt, num_actions,scope, K = 10, reuse=False):
    with tf.name_scope(scope):
        out = inpt
        for i, hidden in enumerate(hiddens):
            out = layers.fully_connected(out, num_outputs=hidden, activation_fn=tf.nn.relu,
                                         scope='hidden_'+str(i+1))

        out = layers.fully_connected(out, num_outputs=num_actions, activation_fn=None)
        return out


def mlp(hiddens=[]):
    """This model takes as input an observation and returns values of all actions.

    Parameters
    ----------
    hiddens: [int]
        list of sizes of hidden layers

    Returns
    -------
    q_func: function
        q_function for DQN algorithm.
    """
    return lambda *args, **kwargs: _mlp(hiddens, *args, **kwargs)


state_dim = 5
actions_num = 7
n = 100

data = np.ones((n, state_dim))
answers = np.ones((n, actions_num))


def learn(X, y, model, max_steps = 50):
    with tf.Session() as sess:

        with tf.name_scope('input'):
            x = tf.placeholder(tf.float32, [None,  state_dim],   name='x-input')
            y_ = tf.placeholder(tf.float32, [None, actions_num], name='y-input')


        train_writer = tf.summary.FileWriter('tinyq', sess.graph)

        optimizer = tf.train.AdamOptimizer(learning_rate=0.1)
        y = model(x, actions_num, scope='my_model')
        #error = tf.metrics.mean_squared_error(y, y_)[0]
        error = tf.losses.mean_squared_error(y, y_)
        tf.summary.scalar('error', error)
        train_step = optimizer.minimize(error)

        merged = tf.summary.merge_all()
        sess.run(tf.global_variables_initializer())

        for i in range(max_steps):
            print(i)
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()

            summary, _ = sess.run([merged, train_step],
                                  feed_dict={x: data[0:2,:], y_: answers[0:2,:]},
                                  options=run_options,
                                  run_metadata=run_metadata)
            train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
            train_writer.add_summary(summary, i)
        train_writer.close()

learn(data, answers, mlp(hiddens=[64, 32, 32]))


