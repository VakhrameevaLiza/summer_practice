import gym
from baselines import deepq
from chain_environment import SimpleChain
import numpy as np
import random
import tensorflow as tf
from _datetime import datetime


def callback(lcl, glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) >= 10  # CHECK
    return is_solved


def policy_evaluate_callback(ns, K, lcl, glb,
                             n_test_episodes=100, window_size=100,
                             mean_flag = True, seed=None):
    test_env = SimpleChain(ns)
    if seed is not None:
        test_env.seed(seed)
    acts = lcl['acts']
    episode_rewards = np.zeros(n_test_episodes)
    start_time = datetime.now()
    for i in range(n_test_episodes):
        obs = test_env.reset()
        while True:
            actions = [acts[k](np.array(obs)[None], stochastic = False)[0] for k in range(K)]
            action = np.bincount(actions).argmax()
            obs, rew, done, _ = test_env.step(action)
            episode_rewards[i] += rew
            if done:
                break
    best = np.convolve(episode_rewards, np.ones(window_size), mode='valid').max()
    print('Total time to evaluate:', (datetime.now() - start_time).total_seconds())
    print(best)
    if mean_flag:
        return best / window_size
    else:
        return best


def count_episodes_for_chain(ns):
    env = SimpleChain(ns)
    model = deepq.models.mlp([64])
    fixK = 20
    act, info = deepq.learn(
        env,
        K=fixK,
        prob_thr=0.5,
        q_func=model,
        lr=0.00025,
        rmsproop=True,
        max_timesteps=100000,
        buffer_size=50000,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
        target_network_update_freq=100,
        evaluate_freq=1000,
        #callback=callback,
        evaluate_callback=lambda lcl, glb: policy_evaluate_callback(ns, fixK, lcl, glb,
                                                                    n_test_episodes=125, mean_flag=False),
        evaluate_thr=10
    )
    return info['num_episodes']


def main(seed, output_name):
    ns_arr = np.linspace(0, 100, 11)[1:6].astype('int')
    counts = np.zeros_like(ns_arr)
    for i in range(ns_arr.shape[0]):
        print(ns_arr[i])
        with tf.Graph().as_default():
            tf.set_random_seed(seed)
            counts[i] = count_episodes_for_chain(ns_arr[i])
            output = open(output_name, 'a')
            output.write(str(counts[i]) + ',')
            output.close()
    output = open(output_name, 'a')
    output.write('\n')
    output.close()


if __name__ == '__main__':
    seeds = [1, 42, 132]
    output_name = 'chain_results-20.csv'

    with open(output_name, 'w') as f:
        print('cleared')

    for seed in seeds:
        main(seed, output_name)
