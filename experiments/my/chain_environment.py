import numpy as np

class ActionSpace:
    def __init__(self, na):
        self.n = na

class ObeservationSpace:
    def __init__(self, ns):
        self.shape = (ns,)

class SimpleChain:
    def __init__(self, ns):
        self.ns = ns
        self.action_space = ActionSpace(2)
        self.observation_space = ObeservationSpace(self.ns)

        self.cur_s = 1
        self.cur_state = np.zeros(self.ns)
        self.cur_state[:self.cur_s+1] = 1

        self.count_steps = 0
        self.reward = 0

    def reset(self):
        self.cur_s = 1
        self.cur_state = np.zeros(self.ns)
        self.cur_state[:self.cur_s + 1] = 1

        self.count_steps = 0
        self.reward = 0

        return self.cur_state

    def step(self, a):
        # 0 forward or loop
        # 1 backward or loop
        self.count_steps += 1
        if a == 0:
            if self.cur_s == self.ns - 1:
                next_state = self.cur_s
                reward = 1
            else:
                next_state = self.cur_s + 1
                reward = 0
        else:
            if self.cur_s == 0:
                next_state = self.cur_s
                reward = 1.0 / 1000
            else:
                next_state = self.cur_s - 1
                reward = 0
        self.reward += reward

        self.cur_s = next_state
        self.cur_state = np.zeros(self.ns)
        self.cur_state[:self.cur_s + 1] = 1

        if self.count_steps == self.ns + 9:
            done = True
        else:
            done = False

        return self.cur_state, reward, done, None

