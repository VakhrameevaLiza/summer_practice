import gym
from datetime import datetime
from baselines import deepq
from baselines.common.atari_wrappers_deprecated import wrap_dqn, ScaledFloatFrame
import numpy as np

def policy_evaluate_callback(K, lcl, glb,
                             n_test_episodes=100, window_size=100,
                             mean_flag = True, seed=None, output='breakout.csv',
                             ):
    test_env = gym.make("Breakout-v4")

    if seed is not None:
        test_env.seed(seed)
    acts = lcl['acts']
    episode_rewards = np.zeros(n_test_episodes)
    start_time = datetime.now()
    for i in range(n_test_episodes):
        obs = test_env.reset()
        while True:
            actions = [acts[k](np.array(obs)[None])[0] for k in range(K)]
            action = np.bincount(actions).argmax()
            obs, rew, done, _ = test_env.step(action)
            episode_rewards[i] += rew
            if done:
                break
    best = np.convolve(episode_rewards, np.ones(window_size), mode='valid').max()
    print('Total time to evaluate:', (datetime.now() - start_time).total_seconds())
    if mean_flag:
        res = best / window_size
    else:
        res = best
    if output is not None:
        with open(output, 'a') as f:
            f.write(str(lcl['t']) + ',' + str(res) + '\n')
    return res



def main():
    env = gym.make("Breakout-v0")
    #env = ScaledFloatFrame(wrap_dqn(env))
    model = deepq.models.cnn_to_mlp(
        convs=[(32, 8, 4), (64, 4, 2), (64, 3, 1)],
        hiddens=[512],
        dueling=False,
    )
    act, info = deepq.learn(
        env,
        K=10,
        q_func=model,
        lr=0.00025,
        batch_size=32,
        max_timesteps=200000000,
        buffer_size=1000000,
        exploration_fraction=0.005,
        exploration_final_eps=0.01,
        train_freq=4,
        learning_starts=10000,
        target_network_update_freq=10000,
        gamma=0.99,
        prioritized_replay=False,
        rmsproop=True,
        momentum=0.95,
        evaluate_callback=lambda lcl, glb: policy_evaluate_callback(10, lcl, glb, seed=42,
                                                                    n_test_episodes=10, mean_flag=True),
        evaluate_freq=1000000,
        print_freq=10
    )
    #act.save("pong_model.pkl")
    #env.close()


if __name__ == '__main__':
    main()
